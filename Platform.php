<?php

namespace App\Libraries;

use Illuminate\Http\Request;
use App\Models\PlatformLog;

use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

class Platform {

    function validate(Request $request){
        $params = $request->all();
        //dd($params);
        //dd($request->header('PK'));
        $response = $this->requestValidation($params,$request->header('PK'));
        if($response['status'] == 'success'){
            $params = $response['params'];
            $method = $params['method'];
            platformLog("Request: " . json_encode($params) . "::ServerInfo: " . json_encode($_SERVER));
            return $response;
        }

        platformLog("Response: " . json_encode($response));


        if(isset($_GET['callback'])){
            echo  trim($_GET['callback'].'('.json_encode($response).');');exit;
        }
        else {
            echo json_encode($response);exit;
        }

    }


    function requestValidation($data,$partner_key){

        $partner_key = 'LB7gn9OF';
        // get app key and salt and other checks
        $sp_details = $this->getSPDetails($partner_key);

        /*echo "<pre>";
        print_r($sp_details);
        exit; */
        if(!count($sp_details)){
            return array('status'=>'failure', 'description'=>'Invalid Partner Key');
        }

        $data1 = $this->dataDecryption($data['req'],$sp_details['salt']);
        $json = json_decode($data1,true);

        //dd($json);
        if(!isset($json['method'])) {
            return array('status'=>'failure', 'code'=>'', 'description'=>'Invalid Request');
        }

        // if(isset($data['document']))$json['document'] = $data['document'];
        $this->logApiRequest($json);

        if(!isset($json['service_id'])) {
            return array('status'=>'failure', 'code'=>'', 'description'=>'Service ID missing');
        }

        if(!array_key_exists($json['service_id'],$sp_details['services'])){
            return array('status'=>'failure', 'description'=>'Invalid Service ID');
        }

        // kit check and service check for pay and login

        if(!$sp_details['services'][$json['service_id']]['toShow']){
            return array('status'=>'failure', 'description'=>'Service is inactive');
        }

        if($json['method'] == 'pay'){
            $products = $sp_details['services'][$json['service_id']]['products'];
            if( !count($products) || !in_array($json['product_id'],array_column($products,'id')) ){
                return array('status'=>'failure', 'description'=>'Invalid Product ID');
            }
        }

        $json['partner_data'] = $sp_details;

        $aclCheck = $this->aclCheck($json);
        if($aclCheck['status'] == 'success'){
            $json['session_data'] = isset($aclCheck['session_data'])?$aclCheck['session_data']:'';
            // $json['token'] = isset($aclCheck['token'])?$aclCheck['token']:'';
        }
        else{
            return $aclCheck;
        }


        if( ($json['method'] != 'deviceInfoUpdate') && in_array($sp_details['services'][$json['service_id']]['registration_type'],array(2,3)) && !empty($json['session_data']) && (!empty($json['session_data']['id'])) ){ // kit based and service based
            $user_id = $json['session_data']['id'];
            $service_id = $json['service_id'];

            $user_service = \App\Models\UserService::where(array('user_id'=>$user_id,'service_id'=>$service_id))->first();
            if( count($user_service) > 0 ){
                if( ($sp_details['services'][$json['service_id']]['registration_type'] == 2) && ($user_service->kit_flag != 1) ){
                    return array('status'=>'failure', 'description'=>'Kit not assigned to user');
                }
                if( ($sp_details['services'][$json['service_id']]['registration_type'] == 2) && ($user_service->service_flag != 1) ){
                    return array('status'=>'failure', 'description'=>'Service not active for user');
                }
                if( ($sp_details['services'][$json['service_id']]['registration_type'] == 3) && ($user_service->service_flag != 1) ){
                    return array('status'=>'failure', 'description'=>'Service not active for user');
                }
            } else {
                return array('status'=>'failure', 'description'=>'Service not active for user');
            }
        }

        $method = $json['method']."Validation";

        // Configure::load('platform');
        // $app_names = Configure::read('app_names');
        // $api_param_counts = Configure::read('api_param_counts');

        $app_names = config('custom.app_names');
        $api_param_counts = config('custom.api_param_counts');

        if(empty($json['app_name']) || !in_array($json['app_name'],$app_names)){
            return array('status'=>'failure', 'code'=>'118', 'description'=>errorDescription(118));
        }
        else if(isset($api_param_counts[$method]) && count($json) != $api_param_counts[$method]){
            return array('status'=>'failure', 'code'=>'119', 'description'=>errorDescription(119));
        }

       if(method_exists($this, $method)){
            $ret = $this->$method($json);
        }
        else {
            $ret = array('status'=>'success');
        }

        if($ret['status'] == 'success'){
            $ret['params'] = $json;
        }


        return $ret;
    }

    function dataDecryption($code,$salt) {
        $key = $salt;
        $hex_iv = '00000000000000000000000000000000';
        $key = hash('sha256', $key, true);
        $td = mcrypt_module_open(MCRYPT_RIJNDAEL_128, '', MCRYPT_MODE_CBC, '');
        mcrypt_generic_init($td, $key, $this->hexToStr($hex_iv));
        $str = mdecrypt_generic($td, base64_decode($code));
        $block = mcrypt_get_block_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC);
        mcrypt_generic_deinit($td);
        mcrypt_module_close($td);
        //print_r($str);
        //dd($this->strippadding($str));
        return $this->strippadding($str);
    }


    function strippadding($string) {
        $slast = ord(substr($string, -1));
        $slastc = chr($slast);
        $pcheck = substr($string, -$slast);
        if (preg_match("/$slastc{" . $slast . "}/", $string)) {
            $string = substr($string, 0, strlen($string) - $slast);
            return $string;
        } else {
            return false;
        }
    }

    function hexToStr($hex)
    {
        $string='';
        for ($i=0; $i < strlen($hex)-1; $i+=2)
        {
            $string .= chr(hexdec($hex[$i].$hex[$i+1]));
        }
        return $string;
    }

    function logApiRequest($data){
        $client_ip = (isset($_SERVER["HTTP_X_FORWARDED_FOR"]) && $_SERVER["HTTP_X_FORWARDED_FOR"] != "") ? $_SERVER["HTTP_X_FORWARDED_FOR"] : $_SERVER["REMOTE_ADDR"];

        if(isset($data['password'])) { $data['password'] = 'xxxx'; }

        if(!isset($data['user_id'])) { $data['user_id'] = null; }

        $this->data['method'] = $data['method'];
        $this->data['params'] = json_encode($data);
        $this->data['user_id'] = $data['user_id'];
        $this->data['ip'] = $client_ip;
        $this->data['timesatmp'] = date('Y-m-d H:i:s');
        $this->data['date'] = date('Y-m-d');
        $this->data['description'] = 'Third Party Login';


        $platform_log = new PlatformLog();
        $platform_log->fill($this->data);
        $platform_log_save = $platform_log->save();

        if($platform_log_save){
            $app_log_id = $platform_log->id;
        }
        $root = isset($_GET['root']) ? $_GET['root'] : "";

        return $app_log_id;
    }
    function aclCheck($data){
        // $data['method'] = 'pay';
        $whitelist_methods = config('custom.whitelist_apis');
        // session check & group id check
        if(in_array($data['method'], $whitelist_methods)) return array('status'=>'success');

        $check_token_res = $this->checkJWTTOken(1);
        if($check_token_res['status'] == 'success'){
            $params['user'] = $check_token_res['user'];

            // $data['user_id'] = 1;
            // if($params['user']['id'] != $data['user_id']) return array('status'=>'failure','code'=>'403','description'=>errorDescription(117));

            $method_mapping= config('custom.acl');
            if(!isset($method_mapping[$data['method']])){
                return array('status'=>'failure','description'=>'Some technical problem. Please try again');
            }
            // $group_ids = explode(',',$val['User']['group_ids']);
            // foreach($group_ids as $group_id) {
            //     if(in_array($group_id,$method_mapping[$method])){
                    return array('status'=>'success', 'code'=>0, 'session_data'=>$params['user']);
            //     }
            // }

        } else {
            return $check_token_res;
        }

        // if($val !== false){
        //     $val = unserialize(substr($val, strpos($val, "Auth|") + 5));
        //     $group_ids = explode(',',$val['User']['group_ids']);
        //     $user_id = $val['User']['id'];
        //     $method = $data['method'];

        //     if($user_id != $data['user_id']) return array('status'=>'failure','code'=>'403','description'=>errorDescription(117));

        //     // Configure::load('platform');
        //     // $method_mapping= Configure::read('acl');
        //     $method_mapping= config('custom.acl');

        //     if(!isset($method_mapping[$method])){
        //         return array('status'=>'failure','description'=>'Some technical problem. Please try again');
        //     }
        //     foreach($group_ids as $group_id) {
        //         if(in_array($group_id,$method_mapping[$method])){
        //             return array('status'=>'success', 'code'=>0, 'session_data'=>$val);
        //         }
        //     }
        // } else {
        //     return array('status'=>'failure','code'=>'404','description'=>errorDescription(404));
        // }

        // return array('status'=>'failure','code'=>'103','description'=>errorDescription(103));
    }


    function __checkUserExist($mobile, $password, $groups,$params = null){
        $params['app_name'] = (!isset($params['app_name'])) ? 'recharge_app' : $params['app_name'];

        $sqlQuery = "SELECT users.*,group_concat(user_groups.group_id) as groupids,user_profile.id  as profile_id FROM users INNER JOIN user_groups ON (users.id =user_groups.user_id) LEFT JOIN user_profile on (user_profile.user_id = users.id and user_profile.uuid = '" . $params['uuid'] . "' And user_profile.app_type ='" . $params['app_name'] . "') WHERE mobile = '" . $mobile . "' AND password = '" . $password . "' AND user_groups.group_id IN (" . implode(",", $groups) . ") group by user_profile.user_id";

        //echo $sqlQuery;exit;
        // $dbObj = ClassRegistry::init('Slaves');
        $data = \DB::select($sqlQuery);
        $data = json_decode(json_encode($data),true);

        /*echo '<pre>';
        print_r($data);
        exit;*/

        if(empty($data)):
            return array('status'=>'failure', 'code'=>'28', 'description'=>errors(28));
        else:
            return array('status'=>'success', 'code'=>0, 'data'=>$data[0]);
        endif;
    }


    function checkAppVersion($appVersionCode,$app_name){

        // Configure::load('platform');
        // $app_versions = Configure::read('app_versions_force_upgrade');

        $app_versions = config('custom.app_versions_force_upgrade');
        if($app_name == 'dmt' && isset($app_versions[$app_name]) && $appVersionCode < $app_versions[$app_name]):
            return array("status"=>"failure", "code"=>"202", "description"=>errorDescription(202), "forced_upgrade_flag" => 1);
        endif;

        if(isset($app_versions[$app_name]) && $appVersionCode < $app_versions[$app_name]):
            return array("status"=>"failure", "code"=>"202", "description"=>errorDescription(202), "forced_upgrade_flag" => 0);
        endif;

        return array("status"=>"success");
    }

    // Send OTP on User Mobile Number for User Device Mapping on Web
    function sendOTPToUserDeviceMapping($mobile, $otp_via_call, $user_id, $lat_long_dist=0){
        $otp = generatePassword(6);
        // $MsgTemplate = $this->General->LoadApiBalance();
        $paramdata['OTP'] = $otp;

        // $dbObj = ClassRegistry::init('Slaves');
        $data = $data = \DB::select("SELECT id FROM users WHERE id = $user_id AND mobile='$mobile'");
        if(empty($data)){
            return array("status"=>"failure", 'code'=>'62', "description"=>'Data is not correct');
        }

        if($lat_long_dist){
            $content = config('custom.Retailer_LocationVerify_OTP_MSG');
        }
        else{
            $content = config('custom.Retailer_DeviceVerify_OTP_MSG');
        }
        $message = ReplaceMultiWord($paramdata, $content);
        logData("api_authenticate_OTP.txt", "in authenticate_new api: " . json_encode($message));
        //dd($message);

        setMemcache("tpkey_otp_userProfileNewUuid_$mobile", $otp, 30 * 60);
        if($otp_via_call == 1){

            // NEED TO LOOK INTO THIS LATER
            // if(getMemcache("tpkey_user_otp_via_call_$mobile")){
            //     return array("status"=>"failure", 'code'=>'62', "description"=>apiErrors('62'));
            // }
            // setMemcache("tpkey_user_otp_via_call_$mobile", $otp_via_call, 1 * 10);
            // curl_post_async("http://click2call.ddns.net/otp.php", array('mobile'=>'2294', 'incoming_route'=>$mobile, 'otp'=>$otp));

            // return array("status"=>"success",'code'=>'101', 'user_id' => $user_id,'description'=>'Request sent successfully. You should receive a call in sometime');
        }


        //dd($otp_via_call);
        sendMessage($mobile,$message);
        return array('status'=>'failure','code'=>'101','user_id' => $user_id,'description'=>'You should receive an OTP in sometime');
     }

    function authenticateValidation($params){

        if( ! isset($params['mobile']) || empty($params['mobile']) ||  ! isset($params['uuid']) || empty($params['uuid'])):
            return array('status'=>'failure', 'code'=>'120', 'description'=>errorDescription(120));
        elseif(mobileValidate($params['mobile']) == '1'):
            return array('status'=>'failure', 'code'=>'115', 'description'=>errorDescription(115));
        elseif(!ctype_alnum(trim($params['uuid']))):
            return array('status' => 'failure','code'=>'116','description' =>'UUID '.errorDescription(116));
        elseif( ! isset($params['version_code']) || empty($params['version_code']) || strlen($params['version_code']) > 5):
            return array('status'=>'failure', 'code'=>'121', 'description'=>errorDescription(121));
        endif;

        return array('status'=>'success');
    }

    function resendOTPAuthenticateValidation($params){

        if( ! isset($params['mobile']) || empty($params['mobile']) ||  ! isset($params['user_id']) || empty($params['user_id'])):
        return array('status'=>'failure', 'code'=>'120', 'description'=>errorDescription(120));
        elseif(mobileValidate($params['mobile']) == '1'): // mobile no validation
        return array('status'=>'failure', 'code'=>'115', 'description'=>errorDescription(115));
        elseif(!ctype_alnum(trim($params['user_id']))):
        return array('status' => 'failure','code'=>'116','description' =>'User id '.errorDescription(116));
        endif;

        return array('status'=>'success');
    }

    function verifyOTPAuthenticateValidation($params){

        if( ! isset($params['mobile']) || empty($params['mobile']) ||  ! isset($params['user_id']) || empty($params['user_id'])):
        return array('status'=>'failure', 'code'=>'120', 'description'=>errorDescription(120));
        elseif(mobileValidate($params['mobile']) == '1'): // mobile no validation
        return array('status'=>'failure', 'code'=>'115', 'description'=>errorDescription(115));
        elseif(!ctype_alnum(trim($params['user_id']))):
        return array('status' => 'failure','code'=>'116','description' =>'User id '.errorDescription(116));
        endif;

        return array('status'=>'success');
    }
    function payValidation($params){

        if( ! isset($params['amount']) || empty($params['amount']) ):
        return array('status'=>'failure', 'description'=>'Amount can not be blank');
        // elseif( ! isset($params['bill_amount']) || empty($params['bill_amount']) ):
        // return array('status'=>'failure', 'description'=>'Bill Amount can not be blank');
        elseif( ! isset($params['txn_id']) || empty($params['txn_id']) ):
        return array('status'=>'failure', 'description'=>'Txn ID missing');
        elseif( ! isset($params['product_id']) || empty($params['product_id']) ):
        return array('status'=>'failure', 'description'=>'Product ID missing');
        endif;

        return array('status'=>'success');
    }

    function deviceInfoUpdateValidation($params){
        if(! isset($params['uuid']) || empty($params['uuid'])):
        return array('status'=>'failure', 'code'=>'28', 'description'=>'Your Mobile number or uuid should not blank');
        elseif(!ctype_alnum(trim($params['uuid']))):
        return array('status' => 'failure','code'=>'116','description' =>'UUID '.errorDescription(116));
        elseif( ! isset($params['version_code']) || empty($params['version_code']) || !floatValidation($params['version_code'])):
        return array('status'=>'failure', 'code'=>'28', 'description'=>'App version code is not valid');
        elseif( ! isset($params['version']) || empty($params['version']) || !floatValidation($params['version'])):
        return array('status'=>'failure', 'code'=>'28', 'description'=>'OS version code is not valid');
        elseif(! isset($params['longitude']) || ! isset($params['latitude'])):
        return array('status'=>'failure', 'code'=>'28', 'description'=>'Latitude & Longitude are missing');
        elseif(!floatValidation($params['longitude']) || !floatValidation($params['latitude'])):
        return array('status'=>'failure', 'code'=>'28', 'description'=>'Latitude & Longitude are not coming right');
        elseif(isset($params['device_manufacturer']) && strlen($params['device_manufacturer']) > 20):
        return array('status'=>'failure', 'code'=>'28', 'description'=>'Device_manufacturer is not valid');

        endif;


        $verify = checkAuthenticateDeviceType($params['device_type']);

        if($verify == 0): // if device_type is not found
            return array('status'=>'failure', 'code'=>'28', 'description'=>errors(28));
        endif;

        return array('status'=>'success');

    }

    function hashPass($password){
        return $this->hash($password,null,true);
    }

    function hash($string, $type = null, $salt = false) {

		if ($salt) {
			if (is_string($salt)) {
				$string = $salt . $string;
			} else {
				$string = config('custom.shopSalt') . $string;
			}
		}

		// if (empty($type)) {
		// 	$type = $_this->hashType;
		// }
		$type = strtolower($type);


		if ($type == 'sha1' || $type == null) {
			if (function_exists('sha1')) {
				$return = sha1($string);
				return $return;
			}
			$type = 'sha256';
		}

		if ($type == 'sha256' && function_exists('mhash')) {
			return bin2hex(mhash(MHASH_SHA256, $string));
		}

		if (function_exists('hash')) {
			return hash($type, $string);
		}
		return md5($string);
	}

    // function getToken($mobile,$password){
    //     $credentials = array('mobile' => $mobile,'password' => $password);
    //     $token = null;
    //     try {
    //         if (!$token = \JWTAuth::attempt($credentials)) {
    //             return response()->json([
    //                 'status' => 'failure',
    //                 'description' => 'invalid Mobile or Password',
    //             ]);
    //         }
    //     } catch (\JWTAuthException $e) {
    //         return response()->json([
    //             'status' => 'failure',
    //             'description' => 'failed_to_create_token',
    //         ]);
    //     }
    //     return response()->json([
    //         'status' => 'success',
    //         'token' => $token
    //     ]);
    // }

    // function getAuthUser(Request $request){
    //     $user = \JWTAuth::toUser($request->token);
    //     return response()->json(['result' => $user]);
    // }
    function getJWTToken($user){
        try {
            $token = \JWTAuth::fromUser($user);
        } catch (\JWTAuthException $e) {
            return array('status' => 'failure','description' => 'Failed to generate token');
        }
        \Cookie::queue(\Cookie::make('token',$token,env('JWT_TTL', 60),'/',null,true,true));
        // header('token:'.encrypt($token));
        $response = $user->toArray();
        $response['user_id'] = $response['id'];
        unset($response['id']);
        $response['status'] = 'success';
        $response['token'] = encrypt($token);

        return $response;
    }

    function checkJWTTOken($refresh = 0){
        try {
            $token = \JWTAuth::getToken();

            if(!$token){
                return array('status' => 'failure','code'=>'404','description' => 'Token missing');
            }
            $user = \JWTAuth::toUser($token);
            if(!$user){
                return array('status'=>'failure','code'=>'404','description'=>errorDescription(404));
            }
        } catch (JWTException $e) {
            if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenInvalidException){
                return array('status' => 'failure','code'=>'404','description' => 'Invalid Token');
            }else if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenExpiredException){
                return array('status' => 'failure','code'=>'404','description' => 'Token Expired');
            }else{
                return array('status' => 'failure','code'=>'404','description' => 'Something went wrong');
            }
        }

        if($refresh){
            $refresh_token = \JWTAuth::refresh($token);
            setcookie('token',encrypt($refresh_token),time()+3600,'/',null,true,true);
            // header('token:'.encrypt($refresh_token));
        } else {
            // setcookie('token',encrypt($token),time()+3600,'/',null,false,true);
        }
        return array('status' => 'success','user' => $user->toArray());
    }
    function destroyJWTTOken(){
        try {
            $token = \JWTAuth::getToken();
            if(!$token){
                return array('status' => 'success');
            }
            \JWTAuth::invalidate($token);
            return array('status' => 'success');
        } catch (JWTException $e) {
            return array('status' => 'success');
        }
    }
    function getAreaIDByLatLong($long,$lat){

        if(!empty($lat) && !empty($long)){
            $loc_data = $this->getAreaByLatLong($long,$lat);
                if(isset($loc_data['state_name']) && !empty($loc_data['state_name'])){
                $loc_data['state_id'] = $this->stateInsert($loc_data['state_name']);
            }
            if(isset($loc_data['city_name']) && !empty($loc_data['city_name'])){
                $loc_data['city_id'] = $this->cityInsert($loc_data['city_name'], $loc_data['state_id']);
            }
            if(isset($loc_data['area_name']) && !empty($loc_data['area_name'])){
                $loc_data['area_id'] = $this->areaInsert($loc_data['area_name'], $loc_data['city_id'],$lat,$long,$loc_data['pincode']);
            }
            else {
                $loc_data['area_id'] = 0;
            }
        }else{
            $loc_data['area_id'] = 0;
        }
        return $loc_data['area_id'];
    }

    function getAreaByLatLong($long,$lat,$cache=true){//$lat,$long
            //19.188421,72
            //0.836591
            if($cache){
                $ret = getMemcache("arealatlong_".round($long,3)."_".round($lat,3));
            }
            else {
                $ret = false;
            }
            $geocode=file_get_contents(config('custom.PAY1_GOOGLE_MAP_API')."&latlng=$lat,$long&sensor=true");
            $output= json_decode($geocode,true);

            if($ret !== false) {
                logData('googleapis.txt',date('Y-m-d H:i:s')."getAreaByLatLong - memcached $lat $long::".json_encode($output)."::".json_encode($ret));
                return $ret;
            }

            $ret = array();
            $ret['area_name'] = "";//empty($output["results"][0]["address_components"][3]["long_name"]) ? "" :$output["results"][0]["address_components"][3]["long_name"];
            $ret['street_number'] = "";//$output["results"][0]["address_components"][0]["long_name"];
            $ret['route'] = "";//$output["results"][0]["address_components"][1]["long_name"];
            $ret['city_name'] = "";//$output["results"][0]["address_components"][5]["long_name"];//city , district
            $ret['state_name'] = "";//$output["results"][0]["address_components"][6]["long_name"];//state
            $ret['country_name'] = "";//$output["results"][0]["address_components"][7]["long_name"];//country
            $ret['pincode'] = "";//$output["results"][0]["address_components"][8]["long_name"];

            foreach($output["results"][0]["address_components"] as $arr){
                //echo "<pre>";print_r($arr);echo "</pre>";
                if(in_array("sublocality_level_1",$arr["types"])){
                    $ret['area_name']           = $arr["long_name"];
                }
                else if(in_array("sublocality_level_2",$arr["types"])){
                    $ret['area_name_1']           = $arr["long_name"];
                }
                else if($arr["types"][0] == "sublocality"){
                    $ret['area_name']           = $arr["long_name"];
                }else if($arr["types"][0] == "locality"){
                    $ret['city_name']  = $arr["long_name"];

                }else if($arr["types"][0] == "administrative_area_level_1"){
                    $ret['state_name'] = $arr["long_name"];
           	 	}
           	 	else if($arr["types"][0] == "administrative_area_level_2"){
                    $ret['extra'] = $arr["long_name"];
                }
                else if($arr["types"][0] == "country"){
                    $ret['country_name']  = $arr["long_name"];

                }else if($arr["types"][0] == "street_number"){
                    $ret['street_number']  = $arr["long_name"];

                }else if($arr["types"][0] == "route"){
                    $ret['route']  = $arr["long_name"];

                }else if($arr["types"][0] == "postal_code"){
                    $ret['pincode']  = $arr["long_name"];

                }
            }

            if(empty($ret['area_name'])){
                if(!empty($ret['area_name_1']))$ret['area_name'] = $ret['area_name_1'];

                if(empty($ret['area_name']) && !empty($ret['extra'])){
                    $ret['area_name'] = $ret['city_name'];
                    $ret['city_name'] = $ret['extra'];
                }
            }

            $ret['formatted_address'] = $output["results"][0]["formatted_address"];
            $ret['lat']  = $output["results"][0]["geometry"]["location"]["lat"];
            $ret['lng']  = $output["results"][0]["geometry"]["location"]["lng"];

            $ret['geoURL']  = config('custom.GOOGLE_MAP_API')."?latlng=$lat,$long&sensor=true";
            setMemcache("arealatlong_".round($long,3)."_".round($lat,3),$ret,7*24*60*60);
            logData('googleapis.txt',date('Y-m-d H:i:s')."getAreaByLatLong $lat $long::".json_encode($output)."::".json_encode($ret));

            return $ret;

        }

         function stateInsert($name ){
            $cityId = 0;
            $mapping = array('Delhi (state)'=>'Delhi');

            // $userObj = ClassRegistry::init('User');
            $name = $this->translate($name);

            $name = isset($mapping[$name]) ? $mapping[$name] : $name;
            $stateInfo = \DB::select("select id , name from locator_state where name like '".$name."'");//echo "select id , name from locator_state where name like '".$name."'";
            $stateInfo = json_decode(json_encode($stateInfo),true);
            if(empty($stateInfo[0]['id'])){
                $lat_long = $this->getLatLongByArea($name);

                $locator_state_data = array(
                    'name'   => $name,
                    'lat'    => $lat_long['lat'],
                    'long'   => $lat_long['lng'],
                    'toShow' => 1
                );
                $locator_state = new LocatorState();
                $locator_state->fill($locator_state_data);
                $locator_state_save = $locator_state->save();

                // $userObj->query("insert into locator_state (  name , lat, `long`, toShow ) values
                // ( '$name', '" . $lat_long['lat'] . "', '" . $lat_long['lng'] . "', 1 ) ;");
                // $stateId = $userObj->query("SELECT LAST_INSERT_ID() as id");
                // $stateId = $stateId[0][0]['id'];
                $stateId = $locator_state->id;
            }else{
                $stateId = $stateInfo[0]['locator_state']['id'];
            }
            return $stateId;
        }

    function cityInsert($name , $stateId){
            $cityId = 0;
            // $userObj = ClassRegistry::init('User');
             $name = $this->translate($name);
            $cityInfo = \DB::select("select id , name from locator_city where name like '".$name."' AND state_id = $stateId;");
            $cityInfo = json_decode(json_encode($cityInfo),true);
            if(empty($cityInfo[0]['id'])){
                $state = \DB::select("SELECT name FROM locator_state WHERE id = $stateId");
                $state = json_decode(json_encode($state),true);
                $lat_long = $this->getLatLongByArea($name . ', ' . $state[0]['name']);

                $locator_city_data = array(
                    'state_id'   => $stateId,
                    'name'   => $name,
                    'lat'    => $lat_long['lat'],
                    'long'   => $lat_long['lng'],
                    'toShow' => 1
                );
                $locator_city = new LocatorCity();
                $locator_city->fill($locator_city_data);
                $locator_city_save = $locator_city->save();

                // $userObj->query("insert into locator_city ( state_id , name , lat, `long`, toShow )
                // values ( $stateId , '$name', '" . $lat_long['lat'] . "', '" . $lat_long['lng'] . "', 1 );");
                // $cityId = $userObj->query("SELECT LAST_INSERT_ID() as id FROM locator_city");
                // $cityId = $cityId[0][0]['id'];
                $cityId = $locator_city->id;
            }else{
                $cityId = $cityInfo[0]['locator_city']['id'];
            }
            return $cityId;
        }

        function areaInsert($name , $cityId,$lat=null,$long=null,$pincode=null){

            // $userObj = ClassRegistry::init('User');
            $name = $this->translate($name);
            $areaInfo = \DB::select("select id , name from locator_area where name like '".$name."' AND city_id = $cityId;");
            $areaInfo = json_decode(json_encode($areaInfo),true);
            if(empty($areaInfo[0]['id'])){
                $city = \DB::select("SELECT name FROM locator_city WHERE id = $cityId");
                $city = json_decode(json_encode($city),true);
                $lat_long = $this->getLatLongByArea($name . ', ' . $city[0]['name']);

                if(empty($lat_long['lat']))$lat_long['lat'] = $lat;
                if(empty($lat_long['lng']))$lat_long['lng'] = $long;
                if(empty($lat_long['pin_code']))$lat_long['pin_code'] = $pincode;

                $locator_area_data = array(
                    'city_id'   => $cityId,
                    'name'   => addslashes($name),
                    'lat'    => $lat_long['lat'],
                    'long'   => $lat_long['lng'],
                    'pincode'   => $lat_long['pin_code'],
                    'toShow' => 1
                );
                $locator_area = new LocatorArea();
                $locator_area->fill($locator_area_data);
                $locator_area_save = $locator_area->save();


                // $userObj->query("insert into locator_area ( city_id, name, lat, `long`, `pincode` , toShow )
                // values ( $cityId , '".addslashes($name)."', '" . $lat_long['lat'] . "', '" . $lat_long['lng'] . "', '" . $lat_long['pin_code'] . "', 1 );");
                // $areaId = $userObj->query("SELECT LAST_INSERT_ID() as id FROM locator_area");
                // $areaId = $areaId[0][0]['id'];
                $areaId = $locator_area->id;
            	logData('/mnt/logs/updateRetailer.txt',"inside areaInsert::area::not in database $ret $areaId");

            }else{
                $areaId = $areaInfo[0]['locator_area']['id'];
                logData('/mnt/logs/updateRetailer.txt',"inside areaInsert::area::inside database".json_encode($areaInfo));

            }
            return $areaId;
        }
    function translate($text){
		preg_match("/[[:ascii:]]+/",$text,$match);
		$name = trim($match[0]);
		//$name = "";
		if(empty($name)){
			$geocode=file_get_contents(config('custom.GOOGLE_TRANSLATE_API')."&q=".urlencode($text)."&target=en");
	        $output= json_decode($geocode,true);
	        $text = $output['data']['translations'][0]['translatedText'];
		}
        //$name = (empty($name)) ? $text : $name;
		return $text;
	}
     function getLatLongByArea($areaLine){//$lat,$long
            //Configure::write('debug',2);
            //echo "Hello";
            $areaLine = urlencode($areaLine);
            $geocode=file_get_contents(config('custom.PAY1_GOOGLE_MAP_API')."&address=$areaLine&sensor=true");

            $output= json_decode($geocode,true);
            //echo "http://maps.googleapis.com/maps/api/geocode/json?address=$areaLine&sensor=true";
            //echo "<pre>"; print_r($output);echo "</pre>";
            $ret = array();
            $ret['address']             = $output["results"][0]["address_components"][0]["long_name"];
            $ret['area_name']           = "";//$output["results"][0]["address_components"][1]["long_name"];
            $ret['city_name']           = "";//$output["results"][0]["address_components"][2]["long_name"];
            $ret['state_name']          = "";//$output["results"][0]["address_components"][3]["long_name"];
            $ret['country_name']        = "";//$output["results"][0]["address_components"][4]["long_name"];
            $ret['formatted_address']   = $output["results"][0]["formatted_address"];
            $ret['lat']  = $output["results"][0]["geometry"]["location"]["lat"];
            $ret['lng']  = $output["results"][0]["geometry"]["location"]["lng"];
            $ret['geoURL']  = config('custom.PAY1_GOOGLE_MAP_API')."&address=$areaLine&sensor=true";

            foreach($output["results"][0]["address_components"] as $arr){
                if(strpos($arr["types"][0],"sublocality") !== false){
                    $ret['area_name']           = $arr["long_name"];
                }else if($arr["types"][0] == "locality"){
                    $ret['city_name']  = $arr["long_name"];
                }else if($arr["types"][0] == "administrative_area_level_1"){
                    $ret['state_name'] = $arr["long_name"];
                }
            	else if($arr["types"][0] == "administrative_area_level_2"){
                    $ret['extra'] = $arr["long_name"];
                }
                else if($arr["types"][0] == "country"){
                    $ret['country_name']  = $arr["long_name"];
                }
                else if($arr["types"][0] == "postal_code"){
                    $ret['pin_code']  = $arr["long_name"];
                }
            }

        	if(!isset($ret['area_name'])){
            	$ret['area_name'] = $ret['city_name'];
            	if(isset($ret['extra']))$ret['city_name'] = $ret['extra'];
            }

            logData('googleapis.txt',date('Y-m-d H:i:s')."getLatLongByArea $areaLine::".json_encode($output)."::".json_encode($ret));

            return $ret;
        }

    function getSPDetails($partner_key = null){
        $sp_details = array();
        if($partner_key){
            //dd($partner_key);
            // $sp_details = getMemcache("sp_data_$partner_key");
            // if(!$sp_details){
                $partner = \App\Models\ServicePartner::with('services')->where(\DB::raw('BINARY `key`'),$partner_key)->first();
                if( count($partner) > 0 ){
                    $services = array();

                    if($partner->services){
                        foreach ($partner->services as $service) {
                            $services[$service['id']] = $service->toArray();
                        }
                    }
                    unset($partner->services);
                    $sp_details = $partner->toArray();
                    $sp_details['services'] = $services;

                    setMemcache("sp_data_$partner_key",$sp_details, 30 * 60);
                }

            // }
        }
        return $sp_details;
    }

    function walletApi($params){
            $return = $this->addWalletTxn($params);
            if($return['status'] == 'failure'){
                return $return;
            }

            $return = $this->makeWalletEntries($params,$return['data']);
            if($return['status'] == 'failure'){
                return $return;
            }

            $settle_flag = isset($params['settle_flag']) ? $params['settle_flag']: 1;
            $this->updateWalletTxn($params['txn_id'],$return['shop_transaction_id'],$return['amt_settled'],$settle_flag,$params['server']);
            return $return;
    }

    function addWalletTxn($params){
        $shop_transaction_id=time().rand();
        $source = -1;
        if(isset($params['source']) && !empty($params['source'])){
            if(trim($params['source']) == 'android') $source = 3;
            else if(trim($params['source']) == 'web') $source = 9;
        }

        $settle_flag = (isset($params['settle_flag'])) ? $params['settle_flag'] : 1;
        $vendor_id = (isset($params['vendor_id'])) ? $params['vendor_id'] : 0;
        $vendor_refid = (isset($params['vendor_refid'])) ? $params['vendor_refid'] : 0;

        if($settle_flag <= 1){
            $settlement_mode = ($settle_flag == 1) ? 0 : 1;

            try {
                $wt = \DB::table('wallets_transactions')->insert([
                    'txn_id' => $params['txn_id'],
                    'shop_transaction_id' => $shop_transaction_id,
                    'server' => $params['server'],
                    'user_id' => $params['user_id'],
                    'product_id' => $params['product_id'],
                    'settlement_mode' => $settlement_mode,
                    'amount' => $params['amount'],
                    'cr_db' => $params['type'],
                    'service_charge' => $params['service_charge'],
                    'commission' => $params['commission'],
                    'tax' => $params['tax'],
                    'description' => $params['description'],
                    'date' => date('Y-m-d'),
                    'created' => date('Y-m-d H:i:s'),
                    'source' => $source,
                    'vendor_id' => $vendor_id,
                    'vendor_refid' => $vendor_refid
                ]);

            } catch(\Illuminate\Database\QueryException $ex){
                if($ex->errorInfo[1] == 1062) {
                    return array('status'=>'failure','errCode'=>'107', 'description'=>walletApiErrorDescription('107'));
                } else {
                    return array('status'=>'failure','errCode'=>'106', 'description'=>walletApiErrorDescription('106'));
                }
            }
            $status = $this->checkWalletTxn($params['txn_id'],$params['server']);
        }
        else {//partial settlement
            $status = $this->checkWalletTxn($params['txn_id'],$params['server']);
            if($status['status'] == 'failure') return $status;
            else if($status['data']['status'] == '2') {
                return array('status'=>'failure','errCode'=>'110', 'description'=>walletApiErrorDescription('110'));
            }
            else if($settle_flag == 2 && date('Y-m-d') > date('Y-m-d',strtotime($status['data']['date'].' +1 day'))){
                return array('status'=>'failure','errCode'=>'117', 'description'=>walletApiErrorDescription('117'));
            }
            else {
                $amt_to_be_settled = $status['data']['amt_remaining_settlement'];
                if($amt_to_be_settled < $params['amount']){
                    return array('status'=>'failure','errCode'=>'116', 'description'=>walletApiErrorDescription('116'));
                }
                if($settle_flag == 3 && $amt_to_be_settled == 0){
                    return array('status'=>'failure','errCode'=>'116', 'description'=>walletApiErrorDescription('116'));
                }
            }
        }

        return $status;
    }

    function checkWalletTxn($txn_id,$server){
        $data = \DB::select("SELECT * FROM wallets_transactions WHERE txn_id = '".$txn_id."' AND server='$server'");
        $data = json_decode(json_encode($data),true);
        if(empty($data)){
           return array('status'=>'failure','errCode'=>'109', 'description'=>walletApiErrorDescription('109'));
        }
        else {
            return array('status'=>'success','data'=>$data[0]);
        }
    }
    function getWalletTxns($date,$server){
        $data = \DB::select("SELECT * FROM wallets_transactions WHERE date = '".$date."' AND server='$server'");
        $data = json_decode(json_encode($data),true);
        if(empty($data)){
           return array('status'=>'failure','errCode'=>'109', 'description'=>walletApiErrorDescription('109'));
        }
        else {
            return array('status'=>'success','data'=>$data);
        }
    }


     function makeWalletEntries($params,$wallet_data){
        $amount = floatval($params['amount']);
        $type = $params['type']; //cr / db
        $product_id = $params['product_id'];
        $service_charge = floatval($params['service_charge']);
        $commission = floatval($params['commission']);
        $tax = floatval($params['tax']); //tax in amount
        $user_id = $params['user_id'];
        $service_id = $params['service_id'];
        $description = $params['description'];
        $txn_id = $params['txn_id'];
        $settle_flag = isset($params['settle_flag']) ? $params['settle_flag']: 1;
        $txn_type = isset($params['txn_type']) ? $params['txn_type'] : 'product';
        $ref_id = isset($params['ref_id']) ? $params['ref_id'] : '';


        if($settle_flag == 2 || $settle_flag == 3){
            if($txn_type != 'product'){
                return array('status'=>'failure','errCode'=>'108','description'=>walletApiErrorDescription('108'));
            }

            if($settle_flag == 3){
                $amount = $wallet_data['amt_remaining_settlement'];
            }
        }

        $other_amount = 0;
        if($service_charge > 0){
            $other_amount -= $service_charge;
            if($tax > 0){
                $tax_desc = "GST";
                $tax_type = config('custom.SERVICE_TAX');
                $other_amount -= $tax;
            }
        }
        else if($commission > 0){
            $other_amount += $commission;
            if($tax > 0){
                $tax_desc = "TDS";
                $tax_type = config('custom.TDS');
                $other_amount -= $tax;
            }
        }

        $opening_bal = $this->getBalance($user_id);

        if($type == 'cr'){
            $amt_to_be_settled = $amount + $other_amount;
            $type_s = 'add';
            $txn_type = (($txn_type == 'incentive') ? config('custom.REFUND') : config('custom.CREDIT_NOTE'));
            $closing_bal = $opening_bal + $amount;
        }
        else if($type == 'db'){
            $amt_to_be_settled = $amount - $other_amount;
            $type_s = 'subtract';
            $txn_type = (($txn_type == 'rental') ? config('custom.RENTAL') : config('custom.DEBIT_NOTE'));
            $closing_bal = $opening_bal - $amount;
        }
        else {
            return array('status'=>'failure','errCode'=>'108','description'=>walletApiErrorDescription('108'));
        }



        if($settle_flag == 0){//no settlement
            $closing_bal = 0;
            $opening_bal = 0;
            $type_flag = 1;
        }
        else if($settle_flag == 1 || $settle_flag == 2){//1 means full settlement in wallet, 2 means partial settlement in wallet
            $clos_bal = $this->shopBalanceUpdate($amt_to_be_settled,$type_s,$user_id,null,null,1,0);
            if($clos_bal === false) return array('status'=>'failure','errCode'=>'105','description'=>walletApiErrorDescription('105'));

            /* if($type == 'cr'){
                $opening_bal = $closing_bal - $amt_to_be_settled;
            }
            else {
                $opening_bal = $closing_bal + $amt_to_be_settled;
            } */

            $type_flag = ($settle_flag == 1) ? 0 : 2;
        }

        if($settle_flag != 3){ // 3 means final settlement in bank
            if($txn_type != config('custom.REFUND')){
                $trans_id = $this->shopTransactionUpdate($txn_type, $amount, $user_id, $product_id, $service_id,null, $type_flag, $description, $opening_bal,$closing_bal,null,null,null);
            } else {
                $trans_id = $this->shopTransactionUpdate($txn_type, $amount, $user_id, null, $service_id,null, $type_flag, $description, $opening_bal,$closing_bal,null,null, null);
            }
            if($trans_id === false) return array('status'=>'failure','errCode'=>'106','description'=>'Transaction entry is not created');

            if($service_charge > 0){
                if($settle_flag == 0){//no settlement
                    $closing_bal = 0;
                    $opening_bal = 0;
                }
                else {
                    $opening_bal = $closing_bal;
                    $closing_bal = $closing_bal-$service_charge;
                }
                $description = "Service Charges - " . $trans_id;
                $ret = $this->shopTransactionUpdate(config('custom.SERVICECHARGES'), $service_charge, $user_id, $trans_id, $service_id, null, $type_flag,$description, $opening_bal, $closing_bal, null, null, null);
                if($ret === false) return array('status'=>'failure','errCode'=>'106','description'=>'Service charge entry not created');

            }


            if($commission > 0){
                if($settle_flag == 0){//no settlement
                    $closing_bal = 0;
                    $opening_bal = 0;
                }
                else {
                    $opening_bal = $closing_bal;
                    $closing_bal = $closing_bal + $commission;
                }
                $description = "Commission - " . $trans_id;
                $ret = $this->shopTransactionUpdate(config('custom.COMMISSION'), $commission, $user_id, $trans_id, $service_id, null, $type_flag, $description, $opening_bal, $closing_bal, null, null,null);
                if($ret === false) return array('status'=>'failure','errCode'=>'106','description'=>'Commission entry not created');

            }

            if($tax > 0){
                if($settle_flag == 0){//no settlement
                    $closing_bal = 0;
                    $opening_bal = 0;
                }
                else {
                    $opening_bal = $closing_bal;
                    $closing_bal = $closing_bal - $tax;
                }
                $description = $tax_desc . " - " .$trans_id;
                $ret = $this->shopTransactionUpdate($tax_type, $tax, $user_id, $trans_id, $service_id, null, $type_flag, $description, $opening_bal , $closing_bal, null,null, null);
                if($ret === false) return array('status'=>'failure','errCode'=>'106','description'=>'TDS entry not created');

            }

            if($txn_type == config('custom.RENTAL')){
                \DB::query("INSERT INTO rentals VALUES (NULL," . $user_id . ",$trans_id,$amount,$amount,$amt_to_be_settled,'','','" . date('Y-m-d') . "')");
            }
            else if($txn_type == config('custom.REFUND')){
                \DB::query("INSERT INTO refunds (user_id,shoptrans_id,amount,type,note,date,timestamp) VALUES (" . $user_id . ",$trans_id,$amt_to_be_settled,3,'" . addslashes($description) . "','" . date('Y-m-d') . "','".date('Y-m-d H:i:s')."')");
            }

        }

        $reference_id = (empty($trans_id)) ? $ref_id : $trans_id;
        $this->makeSettlementEntry($params,$amt_to_be_settled,$reference_id);

        // return array('status'=>'success','closing'=>$closing_bal, 'shop_transaction_id'=>$reference_id, 'amt_settled'=>$amt_to_be_settled, 'type'=>$type);
        return array('status'=>'success','shop_transaction_id'=>$reference_id, 'amt_settled'=>$amt_to_be_settled);
    }

    function getBalance($user_id){
        $qry = "SELECT balance FROM users  WHERE id = $user_id";
        $bal = \DB::select($qry);
        $bal = json_decode(json_encode($bal),true);
        if( ! empty($bal)) return $bal['0']['balance'];
        else return 0;
    }

    function makeSettlementEntry($params,$amt_to_be_settled,$reference_id){
        $settle_flag = isset($params['settle_flag']) ? $params['settle_flag']: 1;
        if($settle_flag == 2 || $settle_flag == 3){
            $settlement_mode = ($settle_flag == 2) ? 0 : 1;
            $sh = \DB::table('settlement_history')->insert([
                    'txn_id' => $params['txn_id'],
                    'server' => $params['server'],
                    'settlement_mode' => $settlement_mode,
                    'settlement_ref_id' => $reference_id,
                    'amount_settled' => $amt_to_be_settled,
                    'date' => date('Y-m-d'),
                    'created' => date('Y-m-d H:i:s')
            ]);
        }
    }

    function shopBalanceUpdate($price, $type, $id, $group_id = null, $dataSource = null, $user_id_flag = 0, $allow_negative = 1){
        // $userObj = is_null($dataSource) ? ClassRegistry::init('User') : $dataSource;

        // if($group_id == SUPER_DISTRIBUTOR){
        // $table = 'super_distributors';
        // }
        // else if($group_id == DISTRIBUTOR){
        // $table = 'distributors';
        // }
        // else if($group_id == SALESMAN){
        // $table = 'salesmen';
        // }
        // else if($group_id == RETAILER){
        // $table = 'retailers';
        // }

        if($type == 'subtract'){
            if($allow_negative == 1){
                \DB::table('users')->decrement('balance', $price, ['id' => $id]);
                // $userObj->query("UPDATE users SET balance = balance - $price WHERE id = $id");
            }
            else
            {

                $query = \DB::table('users')
                            ->where('id',$id)
                            ->where('balance','>=',$price)
                            ->decrement('balance', $price);

                // $userObj->query("UPDATE users SET balance = balance - $price WHERE id = $id AND balance >= $price");
                $affected_rows = \DB::select("SELECT ROW_COUNT() as rows");
                $affected_rows = json_decode(json_encode($affected_rows),true);
                $rows_affected = $affected_rows[0]['rows'];
                if($rows_affected == 0) return false;
            }
        }
        else if($type == 'add'){
            \DB::table('users')->where('id',$id)->increment('balance', $price);
            // $userObj->query("UPDATE users SET balance = balance + $price WHERE id = $id");
        }

        // if($group_id != SALESMAN){
        // $qry = "SELECT " . $table . ".balance,users.mobile FROM $table left join users ON (users.id = user_id) WHERE $table" . ".id = $id";
        // $bal = $userObj->query($qry);
        // }
        // else{
        // $qry = "SELECT balance,mobile FROM $table WHERE id = $id";
        // $bal_1 = $userObj->query($qry);
        // $bal[0][$table] = array('balance'=>$bal_1[0][$table]['balance']);
        // $bal[0]['users'] = array('mobile'=>$bal_1[0][$table]['mobile']);
        // }

        $qry = "SELECT users.balance,users.mobile FROM users  WHERE users.id = $id";
        $bal = \DB::select($qry);
        $bal = json_decode(json_encode($bal),true);

        $final_bal = sprintf('%.2f', $bal['0']['balance']);
        logData('TranQuery' . date('Y-m-d') . '.log', "Retailer Final Bal :  {$final_bal}", FILE_APPEND | LOCK_EX);
        if($final_bal < -50){
            logData('negative_bal.txt', "User ID : $id | Final balance : $final_bal | Mobile number: " . $bal['0']['users']['mobile']);

            // if(is_null($dataSource)): // Send message only if this function is called from somewhere else other than during transaction.
            //     $this->General->sendMessage('9819032643,9820595052,9833258509,7208207549', "Negative balance Alert, User ID : $id | Final balance : $final_bal | Mobile number: " . $bal['0']['users']['mobile'], 'shops');
            //     $this->General->sendMails("Alert : Negative Balance", "User ID : $id <br/> Final balance : $final_bal <br/> Mobile number: " . $bal['0']['users']['mobile'], array('ashish@mindsarray.com', 'chirutha@mindsarray.com','finance@pay1.in'), 'mail');

            //                                             endif;

        }

        return sprintf('%.2f', $bal['0']['balance']);
    }

    function shopTransactionUpdate($type, $amount, $ref1_id, $ref2_id, $user_id = null, $discount = null, $type_flag = null, $note = null, $source_opening = 0, $source_closing = 0, $target_opening = 0, $target_closing = 0, $dataSource = null){
        // if(is_null($dataSource)):

        //     $this->data = null;
        //     // $transObj = ClassRegistry::init('ShopTransaction');

        //     $this->data['ShopTransaction']['source_id'] = (empty($ref1_id) ? 0 : $ref1_id);
        //     $this->data['ShopTransaction']['target_id'] = (empty($ref2_id) ? 0 : $ref2_id);
        //     $this->data['ShopTransaction']['amount'] = $amount;
        //     $this->data['ShopTransaction']['type'] = $type;
        //     $this->data['ShopTransaction']['timestamp'] = date('Y-m-d H:i:s');
        //     $this->data['ShopTransaction']['date'] = date('Y-m-d');
        //     $this->data['ShopTransaction']['note'] = $note;
        //     $this->data['ShopTransaction']['source_opening'] = $source_opening;
        //     $this->data['ShopTransaction']['source_closing'] = $source_closing;
        //     $this->data['ShopTransaction']['target_opening'] = $target_opening;
        //     $this->data['ShopTransaction']['target_closing'] = $target_closing;
        //     if($type == RETAILER_ACTIVATION){
        //         $this->data['ShopTransaction']['confirm_flag'] = 1;
        //     }

        //     if($user_id != null){
        //         $this->data['ShopTransaction']['user_id'] = $user_id;
        //     }
        //     if($discount != null){
        //         $this->data['ShopTransaction']['discount_comission'] = $discount;
        //     }
        //     if($type_flag != null){
        //         $this->data['ShopTransaction']['type_flag'] = $type_flag;
        //     }
        //     $transObj->create();
        //     if($transObj->save($this->data)){
        //         return $transObj->id;
        //     }
        //     else
        //         return false;

        // else:
            // $this->General->logData('/mnt/logs/TranQuery' . date('Y-m-d') . '.log', 'Creating ST ', FILE_APPEND | LOCK_EX);
            $confirm_flag = ($type == config('custom.RETAILER_ACTIVATION')) ? 1 : 0;
            $user_id =  ! is_null($user_id) ? $user_id : null;
            $discount_comission =  ! is_null($discount) ? $discount : null;
            $type_flag =  ! is_null($type_flag) ? $type_flag : null;
            $date = date('Y-m-d');
            $timestamp = date('Y-m-d H:i:s');

            try{

                $st = \DB::table('shop_transactions')->insert([
                    'source_id' => $ref1_id,
                    'target_id' => $ref2_id,
                    'amount' => $amount,
                    'type' => $type,
                    'timestamp' => $timestamp,
                    'date' => $date,
                    'note' => $note,
                    'confirm_flag' => $confirm_flag,
                    'user_id' => $user_id,
                    'discount_comission' => $discount_comission,
                    'type_flag' => $type_flag,
                    'source_opening' => $source_opening,
                    'source_closing' => $source_closing,
                    'target_opening' => $target_opening,
                    'target_closing' => $target_closing
                ]);

                if(in_array($type,array(config('custom.DIST_RETL_BALANCE_TRANSFER'),config('custom.SLMN_RETL_BALANCE_TRANSFER'))))
                {
                    $ret_topup_data = \DB::select("SELECT user_id,topup_flag FROM retailers WHERE id = '$ref2_id' ");
                    $ret_topup_data = json_decode(json_encode($ret_topup_data),true);
                    if(!empty($ret_topup_data) && ($ret_topup_data[0]['topup_flag'] == '0'))
                    {
                        // $dataSource->query("UPDATE retailers SET topup_flag = '1' WHERE id = '$ref2_id' ");

                        \DB::table('retailers')
                            ->where('id',$ref2_id)
                            ->update(['topup_flag' => '1']);

                        //call an api of gamification whenever a user takes balance for the first time
                        // $url = config('custom.REFERRAL_URL')."?user_id=".$ret_topup_data[0]['user_id']."&amount=".$amount."&param1=&param2=";
                        // $data = $this->General->curl_post($url,null,'GET',30,30);
                        logData('referral.txt', "Input : ".$url." Output : ".json_encode($data));
                    }
                }
                $lastInserIdQuery = \DB::select("SELECT id FROM shop_transactions WHERE source_id = '$ref1_id' AND type = '$type' AND date = '$date' AND timestamp = '$timestamp' ORDER BY id DESC LIMIT 1");
                $lastInserIdQuery = json_decode(json_encode($lastInserIdQuery),true);
                logData('TranQuery' . date('Y-m-d') . '.log', "ST id :  {$lastInserIdQuery[0]['id']}", FILE_APPEND | LOCK_EX);
                return $lastInserIdQuery[0]['id'];

            } catch(\Illuminate\Database\QueryException $ex){
                return false;
            }
            // if($dataSource->query($sql)):
                // if(in_array($type,array(config('custom.DIST_RETL_BALANCE_TRANSFER'),config('custom.SLMN_RETL_BALANCE_TRANSFER'))))
                // {
                //     $ret_topup_data = \DB::select("SELECT user_id,topup_flag FROM retailers WHERE id = '$ref2_id' ");
                //     $ret_topup_data = json_decode(json_encode($ret_topup_data),true);
                //     if(!empty($ret_topup_data) && ($ret_topup_data[0]['topup_flag'] == '0'))
                //     {
                //         $dataSource->query("UPDATE retailers SET topup_flag = '1' WHERE id = '$ref2_id' ");

                //         //call an api of gamification whenever a user takes balance for the first time
                //         $url = REFERRAL_URL."?user_id=".$ret_topup_data[0]['user_id']."&amount=".$amount."&param1=&param2=";
                //         $data = $this->General->curl_post($url,null,'GET',30,30);
                //         $this->General->logData('/mnt/logs/referral.txt', "Input : ".$url." Output : ".json_encode($data));
                //     }
                // }
                // $lastInserIdQuery = $dataSource->query("SELECT id FROM shop_transactions WHERE source_id = '$ref1_id' AND type = '$type' AND date = '$date' AND timestamp = '$timestamp' ORDER BY id DESC LIMIT 1");
                // // return $dataSource->lastInsertId();
                // $this->General->logData('/mnt/logs/TranQuery' . date('Y-m-d') . '.log', "ST id :  {$lastInserIdQuery[0][0]['id']}", FILE_APPEND | LOCK_EX);
                // return $lastInserIdQuery[0]['shop_transactions']['id'];

            // else:
            //     $this->General->logData('/mnt/logs/TranQuery' . date('Y-m-d') . '.log', "ST Failed :  {$lastInserIdQuery[0][0]['id']}", FILE_APPEND | LOCK_EX);
            //     return false;
            // endif;

        // endif;
    }

    function updateWalletTxn($txn_id,$shop_txnid,$amt_settled,$settle_flag,$server){
        if($settle_flag == 0 || $settle_flag == 1){
            if($settle_flag == 0){
                $amt_remaining_settlement = $amt_settled;
            }
            else if($settle_flag == 1){
                $amt_remaining_settlement = 0;
            }
            \DB::table('wallets_transactions')
                ->where('txn_id',$txn_id)
                ->where('server',$server)
                ->update([
                    'shop_transaction_id' => $shop_txnid,
                    'amount_settled' => $amt_settled,
                    'amt_remaining_settlement' => $amt_remaining_settlement,
                    'status' => 1
                ]);
        }
        else {
            \DB::table('wallets_transactions')
                    ->where('txn_id',$txn_id)
                    ->where('server',$server)
                    ->where('amt_remaining_settlement',$amt_settled)
                    ->decrement('amt_remaining_settlement',$amt_settled);
        }
    }



    function voidApi($params){

        $return = $this->checkWalletTxn($params['txn_id'],$params['server']);
        if($return['status'] == 'failure'){
            return $return;
        }

        $return = $this->reverseWalletEntries($params,$return['data']);
        if($return['status'] == 'failure'){
            return $return;
        }
        return $return;
    }
    function cancellationRefund($params){

        $return = $this->checkWalletTxn($params['txn_id'],$params['server']);
        if($return['status'] == 'failure'){
            return $return;
        }

        $return = $this->refundAgainstTxn($params,$return['data']);
        if($return['status'] == 'failure'){
            return $return;
        }
        return $return;
    }
    function refundAgainstTxn($params,$wallet_data){
        $shop_txn_id = $wallet_data['shop_transaction_id'];
        $user_id = $params['user_id'];
        $source = $wallet_data['source'];

        $data = \DB::select("SELECT st.amount,st.target_id,st.confirm_flag,st.type,st.source_opening,st.source_closing,st.date FROM shop_transactions as st WHERE st.id = $shop_txn_id");
        $data = json_decode(json_encode($data),true);

        if(empty($data)){
            return array('status'=>'failure','errCode'=>'109','description'=>errorDescription('109'));
        }
        else if($data[0]['type'] == config('custom.CREDIT_NOTE')){
            return array('status'=>'failure','errCode'=>'118','description'=>errorDescription('118'));
        }
        else if($data[0]['date'] < date('Y-m-d',strtotime('-90 days'))){
            return array('status'=>'failure','errCode'=>'118','description'=>errorDescription('118'));
        }
        else if( ($params['refund_amount']+$wallet_data['cancel_refunded_amount']) > $wallet_data['amount_settled'] ){
            return array('status'=>'failure','errCode'=>'118','description'=>errorDescription('118'));
        }
        else {
           $product_id = $data[0]['target_id'];
           $txn_type = $data[0]['type'];
        //    $amt_to_reverse = $wallet_data['amount_settled'] - $wallet_data['amt_remaining_settlement'];
            $amt_to_reverse = $params['refund_amount'];

        //    if($data[0]['type'] == config('custom.CREDIT_NOTE')){
        //        $closing_bal = $this->shopBalanceUpdate($amt_to_reverse,'subtract',$user_id,null,null,1,0);
        //        $opening_bal = $closing_bal + $amt_to_reverse;
        //    }
        //    else
            if($data[0]['type'] == config('custom.DEBIT_NOTE')){
                $closing_bal = $this->shopBalanceUpdate($amt_to_reverse,'add',$user_id,null,null,1,0);
                $opening_bal = $closing_bal - $amt_to_reverse;
            }

           $description = "Txn Cancellation Refund Entry: $shop_txn_id";
           if($closing_bal === false) return array('status'=>'failure','errCode'=>'105','description'=>errorDescription('105'));


           $trans_id = $this->shopTransactionUpdate(config('custom.TXN_CANCEL_REFUND'), $amt_to_reverse, $user_id, $shop_txn_id, $params['service_id'],null,0, $description, $opening_bal, $closing_bal, null,null,null);

           if($trans_id === false) return array('status'=>'failure','errCode'=>'106','description'=>'Transaction entry is not created');

            \DB::table('wallets_transactions')
            ->where('txn_id',$params['txn_id'])
            ->where('server',$params['server'])
            ->increment('cancel_refunded_amount',$amt_to_reverse);

           return array('status'=>'success','closing'=>$closing_bal, 'shop_transaction_id'=>$trans_id, 'amt_settled'=>$amt_to_reverse);
        }
    }

    function reverseWalletEntries($params,$wallet_data){
        $shop_txn_id = $wallet_data['shop_transaction_id'];
        $user_id = $params['user_id'];
        $source = $wallet_data['source'];
        $reversal_charges = (isset($params['reversal_charges'])) ? $params['reversal_charges'] : 0;

        $data = \DB::select("SELECT st.amount,st.target_id,st.confirm_flag,st.type,st.source_opening,st.source_closing,st.date,products.earning_type,products.earning_type_flag FROM shop_transactions as st LEFT JOIN products ON (products.id=st.target_id) WHERE st.id = $shop_txn_id");
        $data = json_decode(json_encode($data),true);

        if(empty($data)){
            return array('status'=>'failure','errCode'=>'109','description'=>errorDescription('109'));
        }
        else if(!$this->lockReverseTransaction($shop_txn_id)){
            return array('status'=>'failure','errCode'=>'110','description'=>errorDescription('110'));
        }
        else if($data[0]['type'] == config('custom.CREDIT_NOTE')){
            return array('status'=>'failure','errCode'=>'118','description'=>errorDescription('118'));
        }
        else if($data[0]['date'] < date('Y-m-d',strtotime('-90 days'))){
            return array('status'=>'failure','errCode'=>'118','description'=>errorDescription('118'));
        }
        else {

           $product_id = $data[0]['target_id'];
           $txn_type = $data[0]['type'];
           $amt_to_reverse = $wallet_data['amount_settled'] - $wallet_data['amt_remaining_settlement'];

            if($reversal_charges > 0 && $amt_to_reverse - $reversal_charges < 0){
               return array('status'=>'failure','errCode'=>'118','description'=>errorDescription('118'));
            }

           if($data[0]['type'] == config('custom.CREDIT_NOTE')){
               $closing_bal = $this->shopBalanceUpdate($amt_to_reverse - $reversal_charges,'subtract',$user_id,null,null,1,0);
               $opening_bal = $closing_bal + $amt_to_reverse - $amt_to_reverse;
               $closing_bal_rev = $opening_bal - $amt_to_reverse;
           }
           else if($data[0]['type'] == config('custom.DEBIT_NOTE')){
               $closing_bal = $this->shopBalanceUpdate($amt_to_reverse - $reversal_charges,'add',$user_id,null,null,1,0);
               $opening_bal = $closing_bal - $amt_to_reverse;
               $closing_bal_rev = $opening_bal + $amt_to_reverse;
           }

           $description = "Reverse Entry: $shop_txn_id";
           if($closing_bal === false) return array('status'=>'failure','errCode'=>'105','description'=>errorDescription('105'));


           $trans_id = $this->shopTransactionUpdate(config('custom.VOID_TXN'), $amt_to_reverse, $user_id, $shop_txn_id, $params['service_id'],null, 0, $description, $opening_bal, $closing_bal, null,null,null);

           if($trans_id === false) return array('status'=>'failure','errCode'=>'106','description'=>'Transaction entry is not created');

            if($data[0]['st']['type'] == config('custom.DEBIT_NOTE') && $reversal_charges > 0){
               $description = "Reversal charges: $trans_id";
               $this->shopTransactionUpdate(config('custom.SERVICECHARGES'), $reversal_charges, $user_id, $trans_id, $params['service_id'],null, 1, $description, $closing_bal_rev, $closing_bal, null,null,null);
            }

           \DB::table('shop_transactions')
            ->where('id',$shop_txn_id)
            ->update([
                'confirm_flag' => 1
            ]);
            \DB::table('shop_transactions')
            ->where('target_id',$shop_txn_id)
            ->update([
                'confirm_flag' => 1
            ]);
           if($data[0]['date'] != date('Y-m-d')){
               $amt = $data[0]['amount'];
               $earning = $amt - $amt_to_reverse;
               $earning = ($earning > 0) ? $earning : (0-$earning);
               $earning_type = $data['1']['earning_type'];
               $earning_type_flag = $data['1']['earning_type_flag'];

               \DB::table('retailer_earning_logs')
                ->where('service_id',$params['service_id'])
                ->where('ret_user_id',$user_id)
                ->where('date',$data[0]['date'])
                ->where('api_flag',$source)
                ->where('type',$txn_type)
                ->where('earning_type',$earning_type)
                ->where('earning_type_flag',$earning_type_flag)
                ->decrement('amount', $amt)
                ->decrement('txn_count', 1)
                ->decrement('earning', $earning);
           }

           $settlement_data = $this->getSettlementEntries($params);
           if($settlement_data['status'] == 'success'){
               foreach($settlement_data['data'] as $set_data){
                   if($set_data['settlement_history']['settlement_mode'] == 0){
                        \DB::table('shop_transactions')
                        ->where('id',$set_data['settlement_history']['settlement_ref_id'])
                        ->update([
                            'confirm_flag' => 1
                        ]);
                   }
               }
           }

            \DB::table('wallets_transactions')
            ->where('txn_id',$params['txn_id'])
            ->where('server',$params['server'])
            ->update([
                'status' => 2,
                'reversal_date' => date('Y-m-d H:i:s')
            ]);
           return array('status'=>'success','closing'=>$closing_bal, 'shop_transaction_id'=>$trans_id, 'amt_settled'=>$amt_to_reverse);
        }

    }
    function lockReverseTransaction($transId){

        $temp_reversed_data = array(
            'shoptrans_id'  => $transId,
            'date'          => date('Y-m-d'),
            'timestamp'     => date('Y-m-d H:i:s')
        );
        $temp_reversed = new \App\Models\TempReversed();
        $temp_reversed->fill($temp_reversed_data);
        try{
            $temp_reversed_save = $temp_reversed->save();
            if($temp_reversed_save){
                return true;
            }
        } catch(\Illuminate\Database\QueryException $ex){
            // if($ex->errorInfo[1] == 1062) {
            //     return array('status'=>'failure','errCode'=>'107', 'description'=>walletApiErrorDescription('107'));
            // } else {
            //     return array('status'=>'failure','errCode'=>'106', 'description'=>walletApiErrorDescription('106'));
            // }
            return false;
        }
        return false;
    }

    function getSettlementEntries($params){
        $data = \DB::select("SELECT * FROM settlement_history WHERE txn_id = '".$params['txn_id']."' AND server='".$params['server']."'");
        $data = json_decode(json_encode($data),true);
        if(empty($data)){
            return array('status'=>'failure','errCode'=>'109', 'description'=>errorDescription('109'));
        }
        else {
            return array('status'=>'success','data'=>$data);
        }
    }


}